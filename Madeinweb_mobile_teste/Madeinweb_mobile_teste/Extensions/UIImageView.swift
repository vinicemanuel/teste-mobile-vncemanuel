//
//  UIImageView.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 17/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView{
    func setImage(From url: String){
        let url = URL(string: url)
        
        self.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "download"))
    }
}
