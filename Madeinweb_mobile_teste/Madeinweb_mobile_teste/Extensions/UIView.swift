//
//  UIView.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 16/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func addBorder(size: CGFloat, color: UIColor){
        self.layer.borderWidth = size
        self.layer.borderColor = color.cgColor
    }
}
