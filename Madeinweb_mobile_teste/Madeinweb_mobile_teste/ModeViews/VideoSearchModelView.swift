//
//  VideoSearchModelView.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 17/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation

class VideoSearchModelView {
    var videoResources: [VideoResourceModelView]
    var nexPageToken: String
    
    var hasNestPage: Bool{
        get{
            return nexPageToken != ""
        }
    }
    
    init(vs: VideoSearch) {
        self.nexPageToken = vs.nextPageToken ?? ""
        self.videoResources = vs.items?.map({VideoResourceModelView(videoResource: $0)}) ?? []
    }
    
    func merge(with other: VideoSearchModelView){
        other.videoResources.forEach { (video) in
            if !self.videoResources.contains(video){
                self.videoResources.append(video)
            }
        }
        self.nexPageToken = other.nexPageToken
    }
}
