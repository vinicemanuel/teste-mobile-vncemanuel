//
//  ThumbnailModelView.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 17/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation

class ThumbnailModelView {
    var url: String
    var width: Int
    var height: Int
    
    init(thumb: Thumbnails) {
        self.url = thumb.url ?? ""
        self.width = thumb.width ?? 0
        self.height = thumb.height ?? 0
    }
}

