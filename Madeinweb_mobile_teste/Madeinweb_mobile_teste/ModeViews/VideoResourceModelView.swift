//
//  VideoResourceModelView.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 17/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation

class VideoResourceModelView: Equatable {
    var id: String
    var title: String
    var description: String
    var channelTitle: String
    var defaultThumbnail: ThumbnailModelView
    var mediumThumbnail: ThumbnailModelView
    var highThumbnail: ThumbnailModelView
    var viewCount: String
    
    init(videoResource: VideoResource) {
        self.id = videoResource.id?.videoId ?? ""
        self.title = videoResource.snippet?.title ?? ""
        self.description = videoResource.snippet?.description ?? ""
        self.channelTitle = videoResource.snippet?.channelTitle ?? ""
        self.defaultThumbnail = ThumbnailModelView(thumb: videoResource.snippet?.thumbnails?.default ?? Thumbnails())
        self.mediumThumbnail = ThumbnailModelView(thumb: videoResource.snippet?.thumbnails?.medium ?? Thumbnails())
        self.highThumbnail = ThumbnailModelView(thumb: videoResource.snippet?.thumbnails?.high ?? Thumbnails())
        self.viewCount = ""
    }
    
    init(uniqueVideoResource: UniqueVideoResource) {
        self.id = uniqueVideoResource.id ?? ""
        self.title = uniqueVideoResource.snippet?.title ?? ""
        self.description = uniqueVideoResource.snippet?.description ?? ""
        self.channelTitle = uniqueVideoResource.snippet?.channelTitle ?? ""
        self.defaultThumbnail = ThumbnailModelView(thumb: uniqueVideoResource.snippet?.thumbnails?.default ?? Thumbnails())
        self.mediumThumbnail = ThumbnailModelView(thumb: uniqueVideoResource.snippet?.thumbnails?.medium ?? Thumbnails())
        self.highThumbnail = ThumbnailModelView(thumb: uniqueVideoResource.snippet?.thumbnails?.high ?? Thumbnails())
        self.viewCount = uniqueVideoResource.statistics?.viewCount ?? ""
    }
    
    static func == (lhs: VideoResourceModelView, rhs: VideoResourceModelView) -> Bool {
        return lhs.id == rhs.id
    }
}
