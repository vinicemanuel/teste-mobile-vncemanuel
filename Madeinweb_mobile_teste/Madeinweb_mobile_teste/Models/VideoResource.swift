//
//  VideoResource.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 17/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation

class VideoResource: Codable {
    var id: VideoRessorceID?
    var snippet: VideoSnippet?
}
