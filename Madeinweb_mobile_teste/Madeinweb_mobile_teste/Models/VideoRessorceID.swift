//
//  VideoRessorceID.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 17/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation

class VideoRessorceID: Codable {
    var kind: String?
    var videoId: String?
}
