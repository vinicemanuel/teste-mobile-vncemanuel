//
//  VideoSnippet.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 17/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation

class VideoSnippet: Codable {
    var publishedAt: String?
    var channelId: String?
    var title: String?
    var description: String?
    var thumbnails: VideoThumbnails?
    var channelTitle: String?
}
