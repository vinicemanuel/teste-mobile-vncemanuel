//
//  MyError.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 17/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation

struct MyError: Error {
    enum Errorkind{
        case noInternet
        case undefined
    }
    
    let kind: Errorkind
    let message: String
    
    init(error: Error) {
        message = error.localizedDescription
        if error._code == -1009{
            self.kind = .noInternet
        }else{
            self.kind = .undefined
        }
    }
}
