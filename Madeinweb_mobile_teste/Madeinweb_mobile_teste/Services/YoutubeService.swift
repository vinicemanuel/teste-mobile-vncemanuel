//
//  YoutubeService.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 16/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import Foundation
import Alamofire

class YoutubeService {
    static let shared = YoutubeService()
    
    private let baseUrl = "https://www.googleapis.com/youtube/v3"
    private lazy var searchUrl = "\(baseUrl)/search"
    private lazy var videoUrl = "\(baseUrl)/videos"
    
    private let videoBaseUrl = "https://www.youtube.com/watch?v="
    
    private let googleKey = "AIzaSyB-vaRP84UixsvhgfwP_977kWckUndcwgo"
    
    func searchVideos(search: String, pageToken: String = "", block: @escaping (_: VideoSearchModelView?, _: MyError?)->Void){
        var param: [String: Any] = [:]
        
        if pageToken == ""{
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                tasks.forEach({$0.cancel()})
            }
        }
        
        param["part"] = "id,snippet"
        param["key"] = googleKey
        param["pageToken"] = pageToken
        param["q"] = search
        param["maxResults"] = 20
        
        Alamofire.request(searchUrl, method: .get, parameters: param).responseData { (response) in
            switch response.result{
            case .failure(let error):
                let myError = MyError(error: error)
                print(error.localizedDescription)
                block(nil, myError)
                
            case .success:
                guard let data = response.result.value else{
                    block(nil, nil)
                    return
                }
                do {
                    let search = try JSONDecoder().decode(VideoSearch.self, from: data)
                    let searchModelView = VideoSearchModelView(vs: search)
                    block(searchModelView, nil)
                } catch let error {
                    let myError = MyError(error: error)
                    block(nil, myError)
                }
            }
        }
    }
    
    func getVideo(videoID: String, block: @escaping (_: VideoResourceModelView?, _: MyError?)->Void){
        var param: [String: Any] = [:]
        
        param["part"] = "snippet,statistics"
        param["key"] = googleKey
        param["id"] = videoID
        
        Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
            tasks.forEach({$0.cancel()})
        }
        
        Alamofire.request(videoUrl, method: .get, parameters: param).responseData { (response) in
            switch response.result{
            case .failure(let error):
                print(error.localizedDescription)
                let myError = MyError(error: error)
                block(nil, myError)
            case .success:
                guard let data = response.result.value else{
                    block(nil, nil)
                    return
                }
                do {
                    let search = try JSONDecoder().decode(UniqueVideoSearch.self, from: data)
                    guard let video = search.items?.first else{
                        block(nil, nil)
                        return
                    }
                    let videoViewModel = VideoResourceModelView(uniqueVideoResource: video)
                    block(videoViewModel, nil)
                } catch let error {
                    print(error.localizedDescription)
                    let myError = MyError(error: error)
                    block(nil, myError)
                }
                
            }
        }
    }
}
