//
//  VideoDetailViewController.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 16/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import UIKit

class VideoDetailViewController: UIViewController {
    
    @IBOutlet weak var videoImg: UIImageView!
    @IBOutlet weak var videoTitle: UILabel!
    @IBOutlet weak var videoVisualizations: UILabel!
    @IBOutlet weak var videoDesc: UITextView!
    
    var videoResourceModelView: VideoResourceModelView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let img = #imageLiteral(resourceName: "madeinweb")
        let imageView = UIImageView(image: img)
        imageView.contentMode = .scaleAspectFit
        imageView.heightAnchor.constraint(equalToConstant: 26.6).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 114.3).isActive = true
        self.navigationItem.titleView = imageView
        
        var numberViews = NSLocalizedString("NUMBER_OF_VIEWS", comment: "views")
        numberViews = String(format: numberViews, self.videoResourceModelView.viewCount)
        
        self.videoTitle.text = self.videoResourceModelView.title
        self.videoDesc.text = self.videoResourceModelView.description
        self.videoVisualizations.text = numberViews
        self.videoImg.setImage(From: self.videoResourceModelView.highThumbnail.url)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "voltar"), for: .normal)
        button.setTitle("voltar", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.7098039216, green: 0.7098039216, blue: 0.7098039216, alpha: 1), for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.5, green: 0.5, blue: 0.5, alpha: 1), for: .highlighted)
        button.imageEdgeInsets.left = -12
        button.addTarget(self, action: #selector(self.back), for: .touchUpInside)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
}

extension VideoDetailViewController{
    @objc func back(){
        self.navigationController?.popViewController(animated: true)
    }
}
