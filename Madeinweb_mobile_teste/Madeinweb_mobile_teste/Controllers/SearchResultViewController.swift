//
//  SearchResultViewController.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 16/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import UIKit

class SearchResultViewController: UIViewController {
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var videoSearchModelView: VideoSearchModelView!
    var text: String!
    
    let cellID = "videoCellID"
    let segueID = "fromListToDetail"
    
    var emptyView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.confirmButton.addBorder(size: 1, color: #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1))
        self.textField.addBorder(size: 1, color: #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1))

        let img = #imageLiteral(resourceName: "madeinweb")
        let imageView = UIImageView(image: img)
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.heightAnchor.constraint(equalToConstant: 26.6).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 114.3).isActive = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.backToSearch))
        imageView.addGestureRecognizer(tap)
        self.navigationItem.titleView = imageView
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.textField.addTarget(self, action: #selector(self.textFielChange(_:)), for: UIControl.Event.editingChanged)
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.textField.text = text
        
        self.emptyView = (UINib(nibName: "EmptyView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationItem.hidesBackButton = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.segueID{
            let videoResourceModelView = sender as! VideoResourceModelView
            let vc = segue.destination as! VideoDetailViewController
            vc.videoResourceModelView = videoResourceModelView
        }
    }
    
    @IBAction func confirmButtonPressed(_ sender: Any) {
        YoutubeService.shared.searchVideos(search: textField.text!) { [unowned self] (videoSearchModelView, error) in
            if let error = error{
                print(error.message)
            }
            
            if let videoSearchModelView = videoSearchModelView{
                self.videoSearchModelView = videoSearchModelView
                self.tableView.reloadData()
            }
        }
    }
}

extension SearchResultViewController{
    @objc func backToSearch(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func textFielChange(_ textField: UITextField) {
        if (textField.text?.count ?? 0) > 0 {
            self.confirmButton.isEnabled = true
            self.confirmButton.backgroundColor = #colorLiteral(red: 0.1505158544, green: 0.5671141744, blue: 0.9033318758, alpha: 1)
        }else{
            self.confirmButton.isEnabled = false
            self.confirmButton.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
    }
}
//MARK: tableView
extension SearchResultViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let number = self.videoSearchModelView.videoResources.count
        
        self.tableView.backgroundView = number == 0 ? self.emptyView : nil
        
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID) as! VideoTableViewCell
        
        let video = self.videoSearchModelView.videoResources[indexPath.row]
        
        cell.videoDesc.text = video.description
        cell.videoTitle.text = video.title
        cell.videoImg.setImage(From: video.highThumbnail.url)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let video = self.videoSearchModelView.videoResources[indexPath.row]
        YoutubeService.shared.getVideo(videoID: video.id, block:{ [unowned self] (videoResource, error) in
            if let error = error{
                print(error.message)
                if error.kind == .noInternet{
                    let alert = UIAlertController(title: "Error", message: error.message, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            if let videoResource = videoResource{
                self.performSegue(withIdentifier: self.segueID, sender: videoResource)
            }
    })
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.videoSearchModelView.videoResources.count - 1 &&
            self.videoSearchModelView.hasNestPage{
            tableView.tableFooterView?.isHidden = false
            self.indicator.startAnimating()
            YoutubeService.shared.searchVideos(search: self.textField.text!, pageToken: self.videoSearchModelView.nexPageToken) { [unowned self] (videoSearchModelView, error) in
                tableView.tableFooterView?.isHidden = true
                self.indicator.stopAnimating()
                if let error = error{
                    print(error.message)
                }
                if let videoSearchModelView = videoSearchModelView{
                    self.videoSearchModelView.merge(with: videoSearchModelView)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
}
