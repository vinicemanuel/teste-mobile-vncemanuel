//
//  ViewController.swift
//  Madeinweb_mobile_teste
//
//  Created by vinicius emanuel on 16/07/19.
//  Copyright © 2019 vinicius emanuel. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    let segueID = "fromSearchToList"
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var confirmButtonCenterContraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textField.addBorder(size: 1, color: #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1))
        self.confirmButton.addBorder(size: 1, color: #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1))
        
        NotificationCenter.default.addObserver(self, selector: #selector(SearchViewController.keyboeardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SearchViewController.keyboeardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.textField.addTarget(self, action: #selector(self.textFielChange(_:)), for: UIControl.Event.editingChanged)
        
        self.confirmButton.isEnabled = false
        self.confirmButton.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        
        self.view.isUserInteractionEnabled = true
        let tapInView = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tapInView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.segueID{
            let videoSearchModelView = sender as! VideoSearchModelView
            let vc = segue.destination as! SearchResultViewController
            vc.videoSearchModelView = videoSearchModelView
            vc.text = self.textField.text
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func confirmButtonPressed(_ sender: Any) {
        self.indicator.startAnimating()
        YoutubeService.shared.searchVideos(search: self.textField.text!) { [unowned self] (result, error) in
            if let error = error{
                print(error.message)
                if error.kind == .noInternet{
                    self.indicator.stopAnimating()
                    let alert = UIAlertController(title: "Error", message: error.message, preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                }
            }
            if let result = result{
                self.indicator.stopAnimating()
                self.performSegue(withIdentifier: self.segueID, sender: result)
            }
        }
    }
}

extension SearchViewController{
    @objc func keyboeardShow(notification: Notification){
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let buttonY = self.confirmButton.frame.origin.y + self.confirmButton.frame.height
            let dif = (keyboardSize.origin.y - buttonY) <= 0 ? (keyboardSize.origin.y - buttonY) : 0
            print("notification: Keyboard will show ", keyboardSize.origin.y, buttonY, dif)

            self.confirmButtonCenterContraint.constant = dif
        }
    }
    
    @objc func keyboeardHide(notification: Notification){
        self.confirmButtonCenterContraint.constant = 0
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @objc func textFielChange(_ textField: UITextField) {
        if (textField.text?.count ?? 0) > 0 {
            self.confirmButton.isEnabled = true
            self.confirmButton.backgroundColor = #colorLiteral(red: 0.1505158544, green: 0.5671141744, blue: 0.9033318758, alpha: 1)
        }else{
            self.confirmButton.isEnabled = false
            self.confirmButton.backgroundColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        }
    }
}

